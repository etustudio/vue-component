'use strict'

export default {
  mounted() {
    if (!this.$options.name) {
      console.warn('To use component-route-mixin, you must specify component name!!!')
    }
    if (this.handleComponentRouted) {
      this.handleComponentRouted()
    }
  },
  computed: {
    isComponentRouted() {
      return this.$router.getMatchedComponents(this.$route).reduce((result, item) => {
        return result || item.name === this.$options.name
      }, false)
    }
  },
  watch: {
    isComponentRouted(to) {
      if (to) {
        if (this.handleComponentRouted) {
          this.handleComponentRouted()
        }
      } else {
        if (this.handleComponentUnrouted) {
          this.handleComponentUnrouted()
        }
      }
    }
  }
}
