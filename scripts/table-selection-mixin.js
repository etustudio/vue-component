'use strict'

import _ from 'lodash'

/**
 * This mixin expose the following things:
 * - tableSection as data field, which as the following fields: selectAll, selectIndeterminate, items
 * - selectedTableItems and firstSelectedTableItem as computed fields
 * - initTableItemsSelection method
 */

function getMixin(exclusive) {
  return {
    data() {
      return {
        tableSelection: {
          selectAll: false,
          selectIndeterminate: false,
          items: [],
          lastSelectedIndex: -1
        }
      }
    },
    computed: {
      selectedTableItems() {
        return this.tableSelection.items.filter(item => item.selected)
      },
      firstSelectedTableItem() {
        return this.selectedTableItems[0] || null
      }
    },
    watch: {
      'tableSelection.selectAll'(to) {
        this.$breakUpdateLoop(() => {
          for (let item of this.tableSelection.items) {
            item.selected = to
          }
          this.selectIndeterminate = false
        })
      },
      'tableSelection.items': {
        deep: true,
        handler() {
          this.$breakUpdateLoop(() => {
            if (exclusive) {
              let selectedIndices = []
              for (let i = 0; i < this.tableSelection.items.length; i++) {
                if (this.tableSelection.items[i].selected) {
                  selectedIndices.push(i)
                }
              }
              if (!selectedIndices.length) {
                this.tableSelection.lastSelectedIndex = -1
              } else if (selectedIndices.length === 1) {
                this.tableSelection.lastSelectedIndex = selectedIndices[0]
              } else {
                let otherSelectedIndex = selectedIndices.filter(
                  item => item !== this.tableSelection.lastSelectedIndex
                )[0]
                for (let index of selectedIndices) {
                  if (index !== otherSelectedIndex) {
                    this.tableSelection.items[index].selected = false
                  }
                }
                this.tableSelection.lastSelectedIndex = otherSelectedIndex
              }
              return
            }
            this._updateTableSelectionForItemsChange()
          })
        }
      }
    },
    methods: {
      _updateTableSelectionForItemsChange() {
        let all = true,
          none = true
        for (let item of this.tableSelection.items) {
          all = all && item.selected
          none = none && !item.selected
        }
        all = all && this.tableSelection.items.length
        if (all) {
          this.tableSelection.selectAll = true
          this.tableSelection.selectIndeterminate = false
        } else if (none) {
          this.tableSelection.selectAll = false
          this.tableSelection.selectIndeterminate = false
        } else {
          this.tableSelection.selectAll = true // bug here with b-checkbox
          this.tableSelection.selectIndeterminate = true
        }
      },
      initTableItemsSelection(items, { idField } = {}) {
        idField = idField || '_id'
        let getId = _.isFunction(idField) ? idField : item => item[idField]
        let lastSelectedIds = []
        for (let item of this.tableSelection.items) {
          if (item.selected && getId(item)) {
            lastSelectedIds.push(getId(item))
          }
        }
        items = items || []
        for (let item of items) {
          item.selected = getId(item) && lastSelectedIds.indexOf(getId(item)) >= 0
        }
        this.$breakUpdateLoop(() => {
          this.tableSelection = {
            selectAll: false,
            selectIndeterminate: false,
            items,
            lastSelectedIndex: -1
          }
          this._updateTableSelectionForItemsChange()
        })
      },
      clearTableItemsSelection() {
        for (let item of this.tableSelection.items) {
          item.selected = false
        }
      }
    }
  }
}

export default getMixin()

export let tableExclusiveSelectionMixin = getMixin(true)
