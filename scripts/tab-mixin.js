'use strict'

export default {
  computed: {
    tab() {
      return this.$route.query.tab || ''
    }
  },
  methods: {
    updateTab(tab) {
      this.$router.replace({ path: this.$route.path, query: tab ? { tab } : null })
    }
  }
}
